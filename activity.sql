/*1. Return the customerName of the customers who are from the Philippines*/
select customerName
from customers
where country = 'Philippines' or
country like '%Philippines%' or 
country like '%ppines';

/*2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"*/
select contactLastName, contactFirstName
From customers
where customerName = 'La Rochelle Gifts';

/*3. Return the product name and MSRP of the product name "The Titanic."*/
select productName, MSRP 
from products
where productName = 'The Titanic';

/*4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"*/
select firstName, lastName 
from employees
where email = 'jfirrelli@classicmodelcars.com';

/*5. Return the names of customers who have no registered state.*/
select * from customers
where state is null or state = '';

/*6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.*/
select firstName, lastName, email
from employees
where lastName = 'Patterson'
and firstName = 'Steve';

/*7. Return customer name, country and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.*/
select customerName, country, creditLimit
from customers a
where a.country != 'USA'
and a.creditLimit > 3000;


/*8. Return the customer numbers of orders whose comments contain the string 'DHL'.*/
select a.customerNumber
from orders a
where a.comments like '%DHL%';

/*9. Return the product lines whose text description mentions the phrase 'state of the art'.*/
select a.productLine 
from productlines a
where a.textDescription like '%state of the art%';

/*10. Return the countries of customers without duplication.*/
select distinct country
from customers a;


/*11. Return the statuses of orders without duplication.*/
select distinct a.status from orders a;

/*12. Return the customer names and countries of customers whose country is USA, France or Canada*/
select customerName, country
from customers a
where a.country in ('USA','France','Canada');

/*13. Return the first name, last name and office's city of employees whose offices are in Tokyo.*/
select b.firstName, b.lastName, a.city
from offices a RIGHT join 
	 employees b on a.officeCode = b.officeCode
where A.city = 'Tokyo';

/*14. Return the customer names of customers who were served by the employee named "Leslie Thompson".*/
select customerName
From customers c left join 
	 employees e on c.salesRepEmployeeNumber = e.employeeNumber
where e.lastName = 'Thompson'
and e.firstName = 'Leslie';

/*15. Return the product names and customer name of products ordered by "Baane Mini Imports".*/
select p.productName, c.customerName 
from customers c 
JOIN orders o on c.customerNumber = o.customerNumber
JOIN orderdetails od on od.orderNumber = o.orderNumber
join products p on p.productCode = od.productCode
where c.customerName = 'Baane Mini Imports';


/*16. Return the employees'first names, employee's last names, customers'names, and offices'countries of transactions whose customers and offices are located in the same country.*/
select e.firstName, e.lastName, c.customerName, o.country
from employees e
JOIN customers c on c.salesRepEmployeeNumber = e.employeeNumber
JOIN offices o on o.officeCode =e.officeCode
where c.country = o.country;


/*17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.*/
SELECT p.productName, p.quantityInStock
FROM products p
JOIN productlines pl ON pl.productLine = p.productLine
WHERE p.productLine = 'planes'
AND p.quantityInStock < 1000;


/*18. Return the customer's name with a phone number containing "+81"*/
select customerName
from customers c
where c.phone LIKE '%+81%';